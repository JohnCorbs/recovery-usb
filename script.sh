#!/bin/sh

# disk to be formatted
disk=/dev/sdc

bootFlags='rw quiet'

packages=''

# package name for kernel version you wish to install
# you can visit the url below and search the term:
# Linux 4.19 for 64-bit PCs (signed)
# or change to 4.19 when new stable kernel drops
# https://packages.debian.org/stable/kernel/
kernel='linux-image-4.19.0-16-amd64'


# url to latest refind-bin
# refindURL='https://downloads.sourceforge.net/project/refind/0.13.2/refind-bin-0.13.2.zip?ts=gAAAAABg4LO98FsQQSNk_CKAUhZOzTSz3nUFRPl_QQ7zl7sPr1YaM112geHq97GToj8m6tYFimg3mGNykKfOLVWAVeDfXO39Rg%3D%3D&r=https%3A%2F%2Fsourceforge.net%2Fprojects%2Frefind%2Ffiles%2F0.13.2%2Frefind-bin-0.13.2.zip%2Fdownload'

#

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi
# https://askubuntu.com/questions/15853/how-can-a-script-check-if-its-being-run-as-root

umount /tmp/recovery-usb/proc
umount /tmp/recovery-usb/dev
umount /tmp/recovery-usb/sys
umount /tmp/recovery-usb
rm -r /tmp/recovery-usb

umount /mnt/proc
umount /mnt/dev
umount /mnt/sys

umount /mnt/boot/efi
umount /mnt

efi="$(echo $disk | awk 'NF{print $0 "1"}')"
root="$(echo $disk | awk 'NF{print $0 "2"}')"

disk () {
	sgdisk -Z $disk
	sgdisk -n 1:0MiB:+500MiB $disk
	sgdisk -N 2 $disk
	sgdisk -c 1:efi -c 2:debianRoot $disk
	sgdisk -t 1:ef00 -t 2:8300 $disk
	
	mkfs.fat -F32 $efi
	mkfs.ext4 $root
}

installRoot() {
	mkdir /tmp/recovery-usb
	mount -t tmpfs -o size=2G tmpfs /tmp/recovery-usb
	debootstrap --arch amd64 buster /tmp/recovery-usb https://deb.debian.org/debian/
}

downloadRefind() {
	mkdir /tmp/refind
	mount -t tmpfs -o size=500M tmpfs /tmp/refind

	# https://stackoverflow.com/a/677212
	# couldnt get curl to work for some reason
	# curl -o refind.zip $refindURL
	# it downloads a file with some text on it
	if command -v wget &> /dev/null
	then
		wget -O /tmp/refind/refind.zip "https://downloads.sourceforge.net/project/refind/0.13.2/refind-bin-0.13.2.zip?ts=gAAAAABg4LO98FsQQSNk_CKAUhZOzTSz3nUFRPl_QQ7zl7sPr1YaM112geHq97GToj8m6tYFimg3mGNykKfOLVWAVeDfXO39Rg%3D%3D&r=https%3A%2F%2Fsourceforge.net%2Fprojects%2Frefind%2Ffiles%2F0.13.2%2Frefind-bin-0.13.2.zip%2Fdownload"
	else
		echo "wget not found"
		exit  1
	fi

	if command -v unzip &> /dev/null
	then
		unzip refind.zip -x "refind-bin-0.13.2/docs" "refind-bin-0.13.2/refind/drivers_aa64" "refind-bin-0.13.2/refind/drivers_ia32" -d /tmp/refind/refind
	else
		echo "unzip not found"
		exit 1
	fi
}

disk | sed 's/^/disk: /' &
installRoot | sed 's/^/installRoot: /' &
downloadRefind | sed 's/^/refind: /' &

wait

mkdir -p /tmp/recovery-usb/boot/efi
mount $efi /tmp/recovery-usb/boot/efi

mount -t proc /proc /tmp/recovery-usb/proc/
mount -t sysfs /sys /tmp/recovery-usb/sys/
mount -o bind /dev /tmp/recovery-usb/dev/

# touch /tmp/recovery-usb/.packages
# echo $kernel $packages > /tmp/recovery-usb/.packages

# cp resources/update-systemd-boot.sh /tmp/recovery-usb/usr/local/bin/update-systemd-boot.sh\
# cp resources/configureBootloader.sh /tmp/recovery-usb/usr/local/bin/configureBootloader.sh
cp resources/chrootScript.sh /tmp/recovery-usb/chrootScript.sh

chmod +x /tmp/recovery-usb/usr/local/bin/configureBootloader.sh

echo "ATTEMPTING TO RUN CHROOT SCRIPT"
chroot /tmp/recovery-usb /bin/bash -c "./chrootScript.sh $kernel $packages"

mkdir -p /tmp/recovery-usb/boot/efi/EFI/BOOT
cp /tmp/refind/refind/refind/* /tmp/recovery-usb/boot/efi/EFI/BOOT
mv /tmp/recovery-usb/boot/efi/EFI/BOOT/refind_x64 /tmp/recovery-usb/boot/efi/EFI/BOOT/BOOTX64


umount /tmp/recovery-usb/proc
umount /tmp/recovery-usb/dev
umount /tmp/recovery-usb/sys

umount $efi
mount $root /mnt
cp -a /tmp/recovery-usb/* /mnt
mount $efi /mnt/boot/efi

mount -t proc /proc /mnt/proc/
mount -t sysfs /sys /mnt/sys/
mount -o bind /dev /mnt/dev/

echo ASDFGHJHJHJKGHVGJKVGFCFCF UGVGHKVGHK

# failed attempt to use systemd boot
# chroot /mnt /bin/bash -c "/usr/local/bin/configurebootloader.sh $bootflags"

# chroot /mnt /bin/bash -c "grub-install --efi-directory=/boot/efi --boot-directory=/boot --removable"
# chroot /mnt /bin/bash -c "update-grub"

umount /mnt/proc
umount /mnt/dev
umount /mnt/sys 
umount /tmp/recovery-usb
