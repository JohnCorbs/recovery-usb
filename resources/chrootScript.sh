#!/bin/bash

echo "RUNNING CHROOT SCRIPT"

# https://stackoverflow.com/questions/1983048/passing-a-string-with-spaces-as-a-function-argument-in-bash
install=$(for i in "$*"; do echo "$i"; done;)


cat <<EOT > /etc/apt/sources.list
deb https://deb.debian.org/debian buster main contrib non-free
deb-src https://deb.debian.org/debian buster main contrib non-free

deb https://deb.debian.org/debian buster-updates main contrib non-free
deb-src https://deb.debian.org/debian buster-updates main contrib non-free

deb http://security.debian.org/ buster/updates main contrib non-free
deb-src http://security.debian.org/ buster/updates main contrib non-free
EOT

apt update
apt -y install $install 
apt autoremove
apt clean

cat <<EOT > /etc/issue
Debian GNU/Linux 10 \n \l

password is password

EOT

echo -e "password\npassword" | passwd root

# mkdir -p /boot/efi/loader/entries
# 
# cat <<EOT > /boot/efi/loader/loader.conf
# default debian
# timeout 1
# editor 1
# EOT

